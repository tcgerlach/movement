package com.talixa.movement.widgets;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import com.talixa.interpolation.EasingFunctions;
import com.talixa.interpolation.Interpolator;
import com.talixa.movement.shared.ImageHelper;
import com.talixa.movement.shared.MovementDemoConstants;

@SuppressWarnings("serial")
public class MovementWidget extends JPanel  {	

	public enum Easing { Linear, EaseIn, EaseOut, EaseInOut, Sin, SinIn, SinOut };
	public enum Function { Interpolation, Bezier3, Bezier4 };

	private static final float ANIMATION_DURATION = 5000;	// 5 seconds to complete one animation
	private long startTime = System.currentTimeMillis();	// time the current animation started

	// screen dimensions
	private static final int WIDTH = 512;
	private static final int HEIGHT = 512;

	private static final int TEXT_PADDING = 2;	// padding for status info at bottom of display
	
	// animation speed
	private static final int FRAMES_PER_SECOND = 60;

	// background image
	private Image starfield;

	// image to animate
	private Image saucer;
	private static final int SAUCER_WIDTH = 50;
	private static final int SAUCER_HEIGHT = 43;

	// points for animation
	private static final float[] P1 = new float[] {0f,0f};									// top left
	private static final float[] P2 = new float[] {WIDTH-SAUCER_WIDTH,0f};					// top right
	private static final float[] P3 = new float[] {0f,HEIGHT-SAUCER_HEIGHT};				// bottom left
	private static final float[] P4 = new float[] {WIDTH-SAUCER_WIDTH,HEIGHT-SAUCER_HEIGHT};// bottom right

	private Easing myEasing = Easing.Linear;				// easing function being used
	private Function func = Function.Interpolation;			// function for finding point

	public MovementWidget() {
		saucer = ImageHelper.getImage(MovementDemoConstants.SAUCER);
		starfield = ImageHelper.getImage(MovementDemoConstants.BACKGROUND);
		
		Timer animation = new Timer(1000/FRAMES_PER_SECOND, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				repaint();
			}
		});
		
		animation.start();
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.draw(g);
	}

	private void draw(Graphics g) {	
		float elapsedTime = System.currentTimeMillis() - startTime;

		// start animation over again if start is less than 0 or animation complete
		if (elapsedTime > ANIMATION_DURATION) {
			startTime = System.currentTimeMillis();
		}

		// calculate point in animation
		float u = elapsedTime / ANIMATION_DURATION;

		// modify value based on desired easing algorithm
		float eu;
		switch (myEasing) {
			case EaseIn: eu = EasingFunctions.easeIn(u, 2); break;
			case EaseOut: eu = EasingFunctions.easeOut(u, 5); break;
			case EaseInOut: eu = EasingFunctions.easeInOut(u, 2); break;
			case Sin: eu = EasingFunctions.sin(u, .3f); break;
			case SinIn: eu = EasingFunctions.sinIn(u); break;
			case SinOut: eu = EasingFunctions.sinOut(u); break;
			case Linear: // FALLTHROUGH
			default: eu = EasingFunctions.linear(u); 
		}

		// determine point 
		float[] point;
		switch (func) {
			case Bezier3: point = Interpolator.bezierCurve(P1, P2, P4, eu); break;
			case Bezier4: point = Interpolator.bezierCurve(P1, P2, P3, P4, eu); break;
			case Interpolation: // FALLTHROUGH
			default: point = Interpolator.interpolate(P1, P4, eu); break;
		}

		// reset background
		// since Mac and Windows show different spacing with the same window size,
		// the background will be repeated so that there is no empty space on the screen
		g.drawImage(starfield, 0, 0, null);
		g.drawImage(starfield, WIDTH, 0, null);
		g.drawImage(starfield, 0, HEIGHT, null);
		g.drawImage(starfield, WIDTH, HEIGHT, null);

		// draw object
		g.drawImage(saucer, (int)point[0], (int)point[1], null);

		// print params
		g.setColor(Color.green);
		g.drawString(func.name() + " " + myEasing.name(), TEXT_PADDING, HEIGHT - TEXT_PADDING);
	}	

	public void setEasing(final Easing newEasing) {
		this.myEasing = newEasing;
	}

	public void setFunction(final Function func) {
		this.func = func;
	}
}

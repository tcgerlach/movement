package com.talixa.movement.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.border.Border;

import com.talixa.movement.shared.MovementDemoConstants;
import com.talixa.movement.listeners.ExitActionListener;
import com.talixa.movement.shared.ImageHelper;

public class FrameAbout {

	private static final int WIDTH = 300;
	private static final int HEIGHT = 200;

	private static final String ABOUT_CONTENTS =
			"<html><center>Sample of sprite movement using linear interpolation, bezier curves, and easing algorithms." +
			"<br><br>Version: " + MovementDemoConstants.VERSION +
			"<br>By Talixa Software</center></html>";

	public static void createAndShowGUI(final JFrame owner) {
		// Create dialog
		JDialog dialog = new JDialog(owner, MovementDemoConstants.TITLE_ABOUT);
		dialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		// set icon
		ImageHelper.setIcon(dialog);

		// set escape listener
		addEscapeListener(dialog);

		// set modality
		dialog.setModal(true);

		// create a content holder with a 10 pixel border
		JPanel holder = new JPanel();
		holder.setLayout(new BorderLayout());
		int border = MovementDemoConstants.BORDER;
		holder.setBorder(BorderFactory.createEmptyBorder(border, border, border, border));
		dialog.add(holder);

		// create components
		ClassLoader cl = ImageHelper.class.getClassLoader();
		JLabel icon = new JLabel(new ImageIcon(cl.getResource("res/saucer.png")));
		holder.add(icon, BorderLayout.WEST);

		JLabel text = new JLabel(ABOUT_CONTENTS);
		Border padding = BorderFactory.createEmptyBorder(border, border, border, border);
		text.setBorder(padding);
		JButton ok = new JButton(MovementDemoConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(dialog));

		// Add to frame
		holder.add(text, BorderLayout.CENTER);
		holder.add(ok, BorderLayout.SOUTH);

		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		dialog.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		dialog.pack();
		dialog.setLocation(left,top);
		dialog.setVisible(true);
	}

	public static void addEscapeListener(final JDialog dialog) {
	    ActionListener escListener = new ActionListener() {
	        public void actionPerformed(final ActionEvent e) {
	            dialog.dispose();
	        }
	    };

	    dialog.getRootPane().registerKeyboardAction(escListener,
	            KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
	            JComponent.WHEN_IN_FOCUSED_WINDOW);
	}

	private FrameAbout() {
		// no constructor - all static
	}
}

package com.talixa.movement.shared;

public class MovementDemoConstants {
	public static final String APPNAME = "Movement Demo";
	public static final String VERSION = "1.0";

	public static final int DEFAULT_FRAME_WIDTH = 500;

	public static final String TITLE_MAIN = APPNAME + " " + VERSION;
	public static final String TITLE_ABOUT = "About " + APPNAME;
	public static final String TITLE_SETTINGS = APPNAME + " Settings";	

	// file menu
	public static final String MENU_FILE = "File";
	public static final String MENU_EXIT = "Exit";

	// easing menu
	public static final String MENU_EASING = "Easing";
	public static final String MENU_LINEAR = "Linear";
	public static final String MENU_EASE_IN = "Ease In";
	public static final String MENU_EASE_IN_OUT = "Ease In Out";
	public static final String MENU_EASE_OUT = "Ease Out";
	public static final String MENU_SIN = "Sin";
	public static final String MENU_SIN_IN = "Sin In";
	public static final String MENU_SIN_OUT = "Sin Out";

	// functions menu
	public static final String MENU_FUNCTION = "Function";
	public static final String MENU_INTERPOLATION = "Interpolation";
	public static final String MENU_BEZIER3 = "Bezier (3-point)";
	public static final String MENU_BEZIER4 = "Bezier (4-point)";

	// help menu
	public static final String MENU_HELP = "Help";	
	public static final String MENU_ABOUT = "About";		

	public static final String LABEL_OK = "Ok";
	public static final String LABEL_CANCEL = "Cancel";

	public static final int APP_WIDTH = 528;
	public static final int APP_HEIGHT = 575;

	public static final String ICON = "res/saucer.png";
	public static final String SAUCER = "res/saucer.png";
	public static final String BACKGROUND = "res/starfield.gif";
			
	public static final int BORDER = 10;

	private MovementDemoConstants() {
		// no constructor
	}
}

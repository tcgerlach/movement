package com.talixa.movement.shared;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;

public class ImageHelper {

	public static void setIcon(final Window w) {
		Image im = getImage(MovementDemoConstants.ICON);
		w.setIconImage(im);
	}

	public static Image getImage(final String name) {
		ClassLoader cl = ImageHelper.class.getClassLoader();	
		return Toolkit.getDefaultToolkit().getImage(cl.getResource(name));
	}

	private ImageHelper() {
		// Hidden constructor
	}
}

package com.talixa.movement;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.movement.frames.FrameAbout;
import com.talixa.movement.listeners.EasingChangedListener;
import com.talixa.movement.listeners.ExitActionListener;
import com.talixa.movement.listeners.FunctionChangedListener;
import com.talixa.movement.shared.MovementDemoConstants;
import com.talixa.movement.widgets.MovementWidget;
import com.talixa.movement.widgets.MovementWidget.Easing;
import com.talixa.movement.widgets.MovementWidget.Function;
import com.talixa.movement.shared.ImageHelper;

public class MovementDemo {
	
	private static JFrame frame;
	private static MovementWidget movement;	
	
	private static void createAndShowGUI() {
		// Create frame
		frame = new JFrame(MovementDemoConstants.TITLE_MAIN);		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
								
		// create movement widget
		movement = new MovementWidget();		
		frame.add(movement, BorderLayout.CENTER);		
			
		// set icon
		ImageHelper.setIcon(frame);	
				
		// Create menus
		addMenus();			
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(MovementDemoConstants.APP_WIDTH,MovementDemoConstants.APP_HEIGHT));
		int left = (screenSize.width/2) - (MovementDemoConstants.APP_WIDTH/2);
		int top  = (screenSize.height/2) - (MovementDemoConstants.APP_HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);		
	}	
	
	private static void addMenus() {
		// Setup file menu
		JMenu fileMenu = new JMenu(MovementDemoConstants.MENU_FILE);
		fileMenu.setMnemonic(KeyEvent.VK_F);	
		addMenuItem(fileMenu, MovementDemoConstants.MENU_EXIT, KeyEvent.VK_X, new ExitActionListener(frame));
		
		// Setup function menu
		JMenu functionMenu = new JMenu(MovementDemoConstants.MENU_FUNCTION);
		functionMenu.setMnemonic(KeyEvent.VK_U);
		addMenuItem(functionMenu, MovementDemoConstants.MENU_INTERPOLATION, KeyEvent.VK_I, new FunctionChangedListener(movement, Function.Interpolation));
		addMenuItem(functionMenu, MovementDemoConstants.MENU_BEZIER3, KeyEvent.VK_3, new FunctionChangedListener(movement, Function.Bezier3));
		addMenuItem(functionMenu, MovementDemoConstants.MENU_BEZIER4, KeyEvent.VK_4, new FunctionChangedListener(movement, Function.Bezier4));
		
		// Setup easing menu
		JMenu easingMenu = new JMenu(MovementDemoConstants.MENU_EASING);
		easingMenu.setMnemonic(KeyEvent.VK_E);
		addMenuItem(easingMenu, MovementDemoConstants.MENU_LINEAR, KeyEvent.VK_L, new EasingChangedListener(movement, Easing.Linear));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_EASE_IN, KeyEvent.VK_I, new EasingChangedListener(movement, Easing.EaseIn));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_EASE_OUT, KeyEvent.VK_O, new EasingChangedListener(movement, Easing.EaseOut));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_EASE_IN_OUT, KeyEvent.VK_E, new EasingChangedListener(movement, Easing.EaseInOut));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_SIN, KeyEvent.VK_S, new EasingChangedListener(movement, Easing.Sin));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_SIN_IN, KeyEvent.VK_N, new EasingChangedListener(movement, Easing.SinIn));
		addMenuItem(easingMenu, MovementDemoConstants.MENU_SIN_OUT, KeyEvent.VK_T, new EasingChangedListener(movement, Easing.SinOut));
		
		// Setup help menu
		JMenu helpMenu = new JMenu(MovementDemoConstants.MENU_HELP);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		addMenuItem(helpMenu, MovementDemoConstants.MENU_ABOUT, KeyEvent.VK_A, new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(frame);
			}
		});
		
		// Add menus to menubar
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(functionMenu);
		menuBar.add(easingMenu);
		menuBar.add(helpMenu);
		frame.setJMenuBar(menuBar);
	}

		
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});				
	}	
	
	private static void addMenuItem(JMenu parent, String name, int mnemonic, ActionListener listener) {
		JMenuItem menuItem = new JMenuItem(name);
		menuItem.setMnemonic(mnemonic);
		menuItem.addActionListener(listener);
		parent.add(menuItem);
	}
}

package com.talixa.movement.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.talixa.movement.widgets.MovementWidget;
import com.talixa.movement.widgets.MovementWidget.Function;

public class FunctionChangedListener implements ActionListener {
	private Function function;
	private MovementWidget movementWidget;

	public FunctionChangedListener(final MovementWidget m, final Function func) {
		this.function = func;
		this.movementWidget = m;
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		movementWidget.setFunction(function);
	}
}

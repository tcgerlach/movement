package com.talixa.movement.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.talixa.movement.widgets.MovementWidget;
import com.talixa.movement.widgets.MovementWidget.Easing;

public class EasingChangedListener implements ActionListener {
	private Easing easing;
	private MovementWidget movementWidget;

	public EasingChangedListener(final MovementWidget m, final Easing e) {
		this.easing = e;
		this.movementWidget = m;
	}

	@Override
	public void actionPerformed(final ActionEvent e) {
		movementWidget.setEasing(easing);
	}
}

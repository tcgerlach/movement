# Purpose
This is a simple Swing project demonstrating the use of linear interpolation, bezier curves, and easing functions to move sprites for game development.

# Dependencies
This project requires my [Interpolation Library](https://bitbucket.org/tcgerlach/interpolation.git). Please build and install this library before attempting to run the movement demo. 